import java.util.Scanner;

import lib.TextIO;

public class PoomisM2ng {

	private static int valestiArvatud;

	public static void main(String[] args) {

		System.out.println("Oled sisenenud poomismängu");
		System.out.print("Sisesta oma nimi: ");
		String m2ngija = TextIO.getln();
		int v6it = 0;
		int kaotus = 0;

		String[] nimisõnad = { "koolikott", "puhkusereis", "hommik", "pannilabidas", "hambahari" };
		String uuesti;

		do {
			String nimisõna = Meetodid.suvalineElement(nimisõnad);

			String kasutajaSõna = "";
			for (int i = 0; i < nimisõna.length(); i++) {
				kasutajaSõna += "*";
			}

			String sisestatudTähed = "";
			// Tükeldab sõna eraldi tähtedeks
			char[] kasutajaSõnaCh = kasutajaSõna.toCharArray();

			// Näitab suvalist sõna peidetud moel
			System.out.println(m2ngija + ", teie jaoks valitud sõna on " + kasutajaSõna);

			// Scanner objekti sisend
			Scanner t2ht = new Scanner(System.in);

			int pikkus = nimisõna.length();
			int oietiArvatud = 0;
			valestiArvatud = 0;
			boolean leiaT2ht;
			char[] nimisõnaT2hed = nimisõna.toCharArray();

			guessLoop: while (pikkus != oietiArvatud) { // GuessLoobiks on kasutatud allikat:
				System.out.print("Sisesta täht: ");// http://stackoverflow.com/questions/14739243/hangman-wrong-guesses-not-printing-correctly
				String arva = t2ht.next();
				char ch = arva.charAt(0);

				leiaT2ht = false;
				for (int i = 0; i < sisestatudTähed.length(); i++) {
					if (ch == sisestatudTähed.charAt(i)) {
						System.out.println("Sa oled pakkunud seda tähte!");
						continue guessLoop;
					}
				} 

				sisestatudTähed += arva;
				for (int i = 0; i < nimisõna.length(); i++) {
					if (nimisõnaT2hed[i] == ch) {
						leiaT2ht = true;
						kasutajaSõnaCh[i] = ch;
						++oietiArvatud;
					}
				} 
				if (!leiaT2ht)
					++valestiArvatud;

				prindiPooja(); // prindib poodava mehikese ekraanile

				if (valestiArvatud == 7) {
					System.out.println(">> Te ei suutnud sõna ära arvata, mehike poodi üles. <<");
					System.out.println("Õige sõna oli: " + nimisõna);
					++kaotus;
					break;
				}

				System.out.println("Teie poolt sisestatud tähed > " + sisestatudTähed + " <");
				// väljastab tähed, mis on kasutaja poolt sisestatud

				System.out.print("Teie sõna on: ");
				for (int j = 0; j < kasutajaSõnaCh.length; j++)
					System.out.print(kasutajaSõnaCh[j]);

				System.out.println();
			}

			if (valestiArvatud < 7) {
				System.out.println(">> Arvasite sõna ära. <<");
				++v6it;
			}

			System.out.println("Teil oli " + valestiArvatud + " valet pakkumist.");

			System.out.println("Kas te soovite uuesti mängida (jah/ei): ");
			uuesti = t2ht.next();

		} while (uuesti.equals("jah"));

		System.out.println(m2ngija + ", teie lõpptulemus on: " + v6it + " võitu " + "ja " + kaotus + " kaotust."); // prindib
																													// välja
																													// lõpptulemuse
	}

	private static void prindiPooja() {

		int vale = valestiArvatud; // valestiarvatud tähtede põhjal joonistab
									// pooja
		int poomisPost = 6; 
		System.out.println("  _ _ ");
		System.out.println(" |   | ");

		if (vale > 0 && vale < 7) {
			System.out.println(" |   | ");
			System.out.println(" |   | ");
		}

		if (vale > 0) {
			System.out.println(" |   O");
			poomisPost = 3;
		}

		if (vale > 1) {
			poomisPost = 2;
			if (vale == 2) {
				System.out.println(" |  /|");
			} else if (vale >= 3) {
				System.out.println(" |  /|\\");
			}
		}

		if (vale >= 4) {
			poomisPost = 1;
			System.out.println(" |   |");
		}

		if (vale > 4) {
			poomisPost = 0;
			if (vale == 5) {
				System.out.println(" |  /");
			} else if (vale >= 6) {
				System.out.println(" |  / \\");
			}
		}

		if (vale == 7) {
			poomisPost = 2;
		}

		for (int k = 0; k < poomisPost; k++) {
			System.out.println(" |");
		}
		System.out.println("_|__");
		System.out.println();
	}
}