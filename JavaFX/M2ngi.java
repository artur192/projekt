import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class M2ngi extends Main {
	static TextField arvatavT2ht = new TextField();
	static Label s6na = new Label("ALUSTA PAKKUMIST NING SINU SÕNA ILMUB");
	static Label pakutudLause = new Label("Siin on juba sinu poolt sisestatud tähed");
	static Label pakutud = new Label();
	static String arvatudT2hed = " ";
	static String nimis6na = S6na.arvatavS6na();
	static int valestiArvatud;
	static int oietiArvatud;
	static Pane timukas = new Pane();
	static Label teade1 = new Label();
	static Label teade = new Label();

	public static Scene m2ngimiseStseen() {

		Label poojaM2ngija = new Label("Poomisega tegeleb " + m2ngija);
		TextField arvatavT2ht = new TextField();

		// Uus lahter, pakutava tähe jaoks
		Pane pane = new Pane();
		pane.getChildren().add(arvatavT2ht);
		arvatavT2ht.setPrefSize(30, 10);
		pane.setTranslateY(120);

		pakutud.setTranslateY(160);
		pakutudLause.setTranslateY(150);

		s6na.setTranslateX(150);
		s6na.setTranslateY(50);
		s6na.setScaleX(1.2);
		s6na.setScaleY(1.2);

		// Luuakse, nupp mis sulgeb akna ja lõpetab mängu
		Button l6petamisNupp = new Button("Lõpeta Mäng");
		l6petamisNupp.setTranslateX(125);
		l6petamisNupp.setTranslateY(245);
		l6petamisNupp.setOnAction(e -> {
			Platform.exit();

		});

		VBox teineAken = new VBox(10);
		teineAken.getChildren().addAll(poojaM2ngija, s6na, pane, pakutudLause, pakutud, postid(), timukas,
				l6petamisNupp);
		arvatavT2ht.setOnKeyReleased(e -> {
			String g = arvatavT2ht.getText().toLowerCase();
			arvatavT2ht.setText("");
			arvatudT2hed += g;
			// Toob esile arvatava sõna
			s6na.setText(t2idaS6na());
			timukas();
			pakutud.setText(arvatudT2hed);
			// System.out.println(valestiArvatud);
		});

		Scene scene = new Scene(teineAken, 600, 500);
		return scene;
	}

	public static String t2idaS6na() {
		/*
		 * Meetodi loomisel kasutasin põhjana koodi alljärgnevalt leheküljelt:
		 * http://stackoverflow.com/questions/32823875/hangman-in-java-fx-
		 * displays-wrong-letters
		 */

		String vastus = "";
		int oige = 0;
		oietiArvatud = 0;
		valestiArvatud = 0;
		int pikkus = nimis6na.length();
		// alljärgneva meetodiga teeb äraarvatava sõna tähtedeks
		for (char l : nimis6na.toCharArray()) {
			// käib läbi äraarvatava sõna ja otsib, kas too sõna sisaldab
			// pakutavat tähte
			if (arvatudT2hed.contains(Character.toString(l))) {
				// System.out.println(vastus);
				vastus += l + " ";
				oietiArvatud = ++oige;
				// System.out.println(oietiArvatud);
			} else {
				vastus += "_ ";

			}
			valestiArvatud = (arvatudT2hed.length() - 1) - oietiArvatud;

			if (oietiArvatud == pikkus) {
				aken.setScene(M2ngi.vaheStseen());
			}
		}

		return vastus;
	}

	private static Scene vaheStseen() {
		/* Luuakse stseen, kui sõna ära arvatakse */
		Label poojaM2ngija = new Label("Poomisega tegeles mängija " + m2ngija);
		if (valestiArvatud == 7) {
			teade1.setText("Kahjuks sa ei arvanud sõna ära, milleks oli: " + nimis6na);
			teade1.setTranslateX(150);
			teade1.setTranslateY(30);
			teade1.setScaleX(1.2);
			teade1.setScaleY(1.2);
		} else {
			teade.setText("Arvasid sõna ära, milleks oli: " + nimis6na);
			teade.setTranslateX(150);
			teade.setTranslateY(30);
			teade.setScaleX(1.2);
			teade.setScaleY(1.2);
		}
		Button uuestiNupp = new Button("Tagasi");
		uuestiNupp.setTranslateX(140);
		uuestiNupp.setTranslateY(36);
		uuestiNupp.setOnAction(e -> {
			vaataTagasi();
		});

		Button l6petamisNupp = new Button("Sulge");
		l6petamisNupp.setTranslateX(400);
		l6petamisNupp.setOnAction(e -> {
			Platform.exit();

		});

		VBox kolmasAken = new VBox(10);
		kolmasAken.getChildren().addAll(poojaM2ngija, teade, teade1, uuestiNupp, l6petamisNupp);

		Scene scene = new Scene(kolmasAken, 600, 200);

		return scene;
	}

	public static void vaataTagasi() {
		aken.setScene(m2ngimiseStseen());

	}

	public static Pane postid() {
		/* Joonistatakse postid, mille otsa timukas puuakse */

		Pane postid = new Pane();

		Line tala = new Line(380, 100, 450, 100);
		postid.getChildren().add(tala);
		Line k8is = new Line(450, 100, 450, 150);
		postid.getChildren().add(k8is);
		Line post = new Line(380, 100, 380, 300);
		postid.getChildren().add(post);
		Line alus = new Line(350, 300, 410, 300);
		postid.getChildren().add(alus);

		return postid;

	}

	public static void timukas() {
		/* Joonistatakse timukas, valedearvamuste põhjal */
		if (valestiArvatud == 1) {
			Circle pea = new Circle(20);
			pea.setTranslateX(450);
			pea.setTranslateY(-120);
			timukas.getChildren().add(pea);
		} else if (valestiArvatud <= 2) {
			Line selg = new Line(450, -100, 450, -50);
			timukas.getChildren().add(selg);
		} else if (valestiArvatud <= 3) {
			Line vK2si = new Line(450, -85, 490, -80);
			timukas.getChildren().add(vK2si);
		} else if (valestiArvatud <= 4) {
			Line pK2si = new Line(450, -85, 410, -80);
			timukas.getChildren().add(pK2si);
		} else if (valestiArvatud <= 5) {
			Line pJalg = new Line(450, -50, 475, 0);
			timukas.getChildren().add(pJalg);
		} else if (valestiArvatud <= 6) {
			Line vJalg = new Line(450, -50, 425, 0);
			timukas.getChildren().add(vJalg);
		} else if (valestiArvatud == 7) {
			aken.setScene(M2ngi.vaheStseen());
		}

	}
}