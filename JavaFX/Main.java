import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

	static Stage aken;
	static String m2ngija;
	Scene stseen;
	static Button edasiNupp;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		aken = primaryStage;
		aken.setTitle("Poomismäng");

		Label m2ngijaKüsimus = new Label("Sisesta oma nimi");
		TextField m2ngijaNimi = new TextField();

		edasiNupp = new Button("Alusta mängimist");
		edasiNupp.setOnAction(e -> {
			m2ngija = m2ngijaNimi.getText();
			aken.setScene(M2ngi.m2ngimiseStseen());
		});

		/*
		 * Lisatakse esimesse aknasse küsimus, mängija nime sisestusälja ja nupp
		 * steeni vahetamiseks
		 */
		VBox alustusAken = new VBox(20);
		alustusAken.getChildren().addAll(m2ngijaKüsimus, m2ngijaNimi, edasiNupp);

		stseen = new Scene(alustusAken, 300, 250);
		aken.setScene(stseen);
		aken.show();

	}

}