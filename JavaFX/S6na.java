
public class S6na {
	
	private static String[] nimis6nad = { "suvaline", "pudel", "kampsun", "jogurt", "touchpad" };
	
	public static String suvalineElement(String[] s6nad) {
		int suvalineIndex = suvalineArv(0, s6nad.length - 1);
		return s6nad[suvalineIndex];
	} 

	public static int suvalineArv(int min, int max) {
		int vahemik = max - min + 1;
		return min + (int) (Math.random() * vahemik);
	} 	//Kasutatud praktikumi materjale
	
	public static String arvatavS6na() {
		return suvalineElement(nimis6nad);
	}

}
